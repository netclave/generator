module gitlab.com/netclave/generator

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/golang/protobuf v1.4.2
	github.com/pquerna/otp v1.2.0
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	github.com/yeqown/go-qrcode v1.2.1
	gitlab.com/netclave/apis v0.0.0-20201019094245-ab92145b89c1
	gitlab.com/netclave/bluetooth-library v0.0.0-20201019080515-f2239dbf2caf
	gitlab.com/netclave/common v0.0.0-20210117131709-40a32e1bcd10
	google.golang.org/grpc v1.31.0
	google.golang.org/protobuf v1.24.0
)
